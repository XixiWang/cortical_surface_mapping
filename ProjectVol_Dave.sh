#! bin/bash
echo "Basch script to project functional volumes to cortical surface"

# export SUBJECTS_DIR=~/Dropbox/LAB_Folder_Macbook/MATLAB/Code/Cortical_Surface_Mapping
export SUBJECTS_DIR=~/Documents/Code/cortical_surface_mapping
export func_dir=${SUBJECTS_DIR}/volumes_anat_only
echo ${func_dir}

# Cortical surface extraction using Freesurfer
echo "Perform cortical surface extraction"
recon-all -s cortical_surf_test2 -i ${func_dir}/T1.nii -all


