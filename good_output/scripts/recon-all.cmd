#-------------------------------------
#@# EM Registration Sat May 21 10:27:01 EDT 2011

 mri_em_register -uns 3 -mask brainmask.mgz nu.mgz /space/freesurfer/centos4.0_x86_64/stable5/average/RB_all_2008-03-26.gca transforms/talairach.lta 

#--------------------------------------
#@# CA Normalize Sat May 21 10:51:05 EDT 2011

 mri_ca_normalize -c ctrl_pts.mgz -mask brainmask.mgz nu.mgz /space/freesurfer/centos4.0_x86_64/stable5/average/RB_all_2008-03-26.gca transforms/talairach.lta norm.mgz 

#--------------------------------------
#@# CA Reg Sat May 21 10:53:03 EDT 2011

 mri_ca_register -nobigventricles -T transforms/talairach.lta -align-after -mask brainmask.mgz norm.mgz /space/freesurfer/centos4.0_x86_64/stable5/average/RB_all_2008-03-26.gca transforms/talairach.m3z 

#--------------------------------------
#@# CA Reg Inv Sat May 21 15:58:57 EDT 2011

 mri_ca_register -invert-and-save transforms/talairach.m3z 

#--------------------------------------
#@# Remove Neck Sat May 21 16:00:08 EDT 2011

 mri_remove_neck -radius 25 nu.mgz transforms/talairach.m3z /space/freesurfer/centos4.0_x86_64/stable5/average/RB_all_2008-03-26.gca nu_noneck.mgz 

#--------------------------------------
#@# SkullLTA Sat May 21 16:01:29 EDT 2011

 mri_em_register -skull -t transforms/talairach.lta nu_noneck.mgz /space/freesurfer/centos4.0_x86_64/stable5/average/RB_all_withskull_2008-03-26.gca transforms/talairach_with_skull.lta 

#--------------------------------------
#@# SubCort Seg Sat May 21 16:22:30 EDT 2011

 mri_seg_diff --seg1 aseg.auto.mgz --seg2 aseg.mgz --diff aseg.manedit.mgz 


 mri_ca_label -align -nobigventricles norm.mgz transforms/talairach.m3z /space/freesurfer/centos4.0_x86_64/stable5/average/RB_all_2008-03-26.gca aseg.auto_noCCseg.mgz 


 mri_cc -aseg aseg.auto_noCCseg.mgz -o aseg.auto.mgz -lta /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/good_output/mri/transforms/cc_up.lta good_output 

#--------------------------------------
#@# Merge ASeg Sat May 21 16:43:33 EDT 2011

 cp aseg.auto.mgz aseg.mgz 

#--------------------------------------------
#@# Intensity Normalization2 Sat May 21 16:43:34 EDT 2011

 mri_normalize -aseg aseg.mgz -mask brainmask.mgz norm.mgz brain.mgz 

#--------------------------------------------
#@# Mask BFS Sat May 21 16:47:15 EDT 2011

 mri_mask -T 5 brain.mgz brainmask.mgz brain.finalsurfs.mgz 

#--------------------------------------------
#@# WM Segmentation Sat May 21 16:47:19 EDT 2011

 mri_binarize --i wm.mgz --min 255 --max 255 --o wm255.mgz --count wm255.txt 


 mri_binarize --i wm.mgz --min 1 --max 1 --o wm1.mgz --count wm1.txt 


 rm wm1.mgz wm255.mgz 


 mri_segment -keep brain.mgz wm.seg.mgz 


 mri_edit_wm_with_aseg -keep-in wm.seg.mgz brain.mgz aseg.mgz wm.asegedit.mgz 


 mri_pretess -keep wm.asegedit.mgz wm norm.mgz wm.mgz 

#--------------------------------------------
#@# Fill Sat May 21 16:50:01 EDT 2011

 mri_fill -a ../scripts/ponscc.cut.log -xform transforms/talairach.lta -segmentation aseg.auto_noCCseg.mgz wm.mgz filled.mgz 

#--------------------------------------------
#@# Tessellate lh Sat May 21 16:50:58 EDT 2011

 mri_pretess ../mri/filled.mgz 255 ../mri/norm.mgz ../mri/filled-pretess255.mgz 


 mri_tessellate ../mri/filled-pretess255.mgz 255 ../surf/lh.orig.nofix 


 rm -f ../mri/filled-pretess255.mgz 


 mris_extract_main_component ../surf/lh.orig.nofix ../surf/lh.orig.nofix 

#--------------------------------------------
#@# Smooth1 lh Sat May 21 16:51:08 EDT 2011

 mris_smooth -nw -seed 1234 ../surf/lh.orig.nofix ../surf/lh.smoothwm.nofix 

#--------------------------------------------
#@# Inflation1 lh Sat May 21 16:51:13 EDT 2011

 mris_inflate -no-save-sulc ../surf/lh.smoothwm.nofix ../surf/lh.inflated.nofix 

#--------------------------------------------
#@# QSphere lh Sat May 21 16:51:56 EDT 2011

 mris_sphere -q -seed 1234 ../surf/lh.inflated.nofix ../surf/lh.qsphere.nofix 

#--------------------------------------------
#@# Fix Topology lh Sat May 21 16:57:09 EDT 2011

 cp ../surf/lh.orig.nofix ../surf/lh.orig 


 cp ../surf/lh.inflated.nofix ../surf/lh.inflated 


 mris_fix_topology -mgz -sphere qsphere.nofix -ga -seed 1234 good_output lh 


 mris_euler_number ../surf/lh.orig 


 mris_remove_intersection ../surf/lh.orig ../surf/lh.orig 


 rm ../surf/lh.inflated 

#--------------------------------------------
#@# Make White Surf lh Sat May 21 17:12:44 EDT 2011

 mris_make_surfaces -noaparc -whiteonly -mgz -T1 brain.finalsurfs good_output lh 

#--------------------------------------------
#@# Smooth2 lh Sat May 21 17:18:33 EDT 2011

 mris_smooth -n 3 -nw -seed 1234 ../surf/lh.white ../surf/lh.smoothwm 

#--------------------------------------------
#@# Inflation2 lh Sat May 21 17:18:38 EDT 2011

 mris_inflate ../surf/lh.smoothwm ../surf/lh.inflated 


 mris_curvature -thresh .999 -n -a 5 -w -distances 10 10 ../surf/lh.inflated 

#--------------------------------------------
#@# Sphere lh Sat May 21 17:20:42 EDT 2011

 mris_sphere -seed 1234 ../surf/lh.inflated ../surf/lh.sphere 

#--------------------------------------------
#@# Surf Reg lh Sat May 21 18:16:41 EDT 2011

 mris_register -curv ../surf/lh.sphere /space/freesurfer/centos4.0_x86_64/stable5/average/lh.average.curvature.filled.buckner40.tif ../surf/lh.sphere.reg 

#--------------------------------------------
#@# Jacobian white lh Sat May 21 18:51:35 EDT 2011

 mris_jacobian ../surf/lh.white ../surf/lh.sphere.reg ../surf/lh.jacobian_white 

#--------------------------------------------
#@# AvgCurv lh Sat May 21 18:51:38 EDT 2011

 mrisp_paint -a 5 /space/freesurfer/centos4.0_x86_64/stable5/average/lh.average.curvature.filled.buckner40.tif#6 ../surf/lh.sphere.reg ../surf/lh.avg_curv 

#-----------------------------------------
#@# Cortical Parc lh Sat May 21 18:51:41 EDT 2011

 mris_ca_label -l ../label/lh.cortex.label -aseg ../mri/aseg.mgz -seed 1234 good_output lh ../surf/lh.sphere.reg /space/freesurfer/centos4.0_x86_64/stable5/average/lh.curvature.buckner40.filled.desikan_killiany.2010-03-25.gcs ../label/lh.aparc.annot 

#--------------------------------------------
#@# Make Pial Surf lh Sat May 21 18:52:33 EDT 2011

 mris_make_surfaces -white NOWRITE -mgz -T1 brain.finalsurfs good_output lh 

#--------------------------------------------
#@# Surf Volume lh Sat May 21 19:03:20 EDT 2011

 mris_calc -o lh.area.mid lh.area add lh.area.pial 


 mris_calc -o lh.area.mid lh.area.mid div 2 


 mris_calc -o lh.volume lh.area.mid mul lh.thickness 

#-----------------------------------------
#@# Parcellation Stats lh Sat May 21 19:03:23 EDT 2011

 mris_anatomical_stats -mgz -cortex ../label/lh.cortex.label -f ../stats/lh.aparc.stats -b -a ../label/lh.aparc.annot -c ../label/aparc.annot.ctab good_output lh white 

#-----------------------------------------
#@# Cortical Parc 2 lh Sat May 21 19:03:38 EDT 2011

 mris_ca_label -l ../label/lh.cortex.label -aseg ../mri/aseg.mgz -seed 1234 good_output lh ../surf/lh.sphere.reg /space/freesurfer/centos4.0_x86_64/stable5/average/lh.destrieux.simple.2009-07-29.gcs ../label/lh.aparc.a2009s.annot 

#-----------------------------------------
#@# Parcellation Stats 2 lh Sat May 21 19:04:36 EDT 2011

 mris_anatomical_stats -mgz -cortex ../label/lh.cortex.label -f ../stats/lh.aparc.a2009s.stats -b -a ../label/lh.aparc.a2009s.annot -c ../label/aparc.annot.a2009s.ctab good_output lh white 

#--------------------------------------------
#@# Tessellate rh Sat May 21 19:04:52 EDT 2011

 mri_pretess ../mri/filled.mgz 127 ../mri/norm.mgz ../mri/filled-pretess127.mgz 


 mri_tessellate ../mri/filled-pretess127.mgz 127 ../surf/rh.orig.nofix 


 rm -f ../mri/filled-pretess127.mgz 


 mris_extract_main_component ../surf/rh.orig.nofix ../surf/rh.orig.nofix 

#--------------------------------------------
#@# Smooth1 rh Sat May 21 19:05:03 EDT 2011

 mris_smooth -nw -seed 1234 ../surf/rh.orig.nofix ../surf/rh.smoothwm.nofix 

#--------------------------------------------
#@# Inflation1 rh Sat May 21 19:05:08 EDT 2011

 mris_inflate -no-save-sulc ../surf/rh.smoothwm.nofix ../surf/rh.inflated.nofix 

#--------------------------------------------
#@# QSphere rh Sat May 21 19:05:51 EDT 2011

 mris_sphere -q -seed 1234 ../surf/rh.inflated.nofix ../surf/rh.qsphere.nofix 

#--------------------------------------------
#@# Fix Topology rh Sat May 21 19:12:10 EDT 2011

 cp ../surf/rh.orig.nofix ../surf/rh.orig 


 cp ../surf/rh.inflated.nofix ../surf/rh.inflated 


 mris_fix_topology -mgz -sphere qsphere.nofix -ga -seed 1234 good_output rh 


 mris_euler_number ../surf/rh.orig 


 mris_remove_intersection ../surf/rh.orig ../surf/rh.orig 


 rm ../surf/rh.inflated 

#--------------------------------------------
#@# Make White Surf rh Sat May 21 19:26:18 EDT 2011

 mris_make_surfaces -noaparc -whiteonly -mgz -T1 brain.finalsurfs good_output rh 

#--------------------------------------------
#@# Smooth2 rh Sat May 21 19:32:16 EDT 2011

 mris_smooth -n 3 -nw -seed 1234 ../surf/rh.white ../surf/rh.smoothwm 

#--------------------------------------------
#@# Inflation2 rh Sat May 21 19:32:20 EDT 2011

 mris_inflate ../surf/rh.smoothwm ../surf/rh.inflated 


 mris_curvature -thresh .999 -n -a 5 -w -distances 10 10 ../surf/rh.inflated 

#--------------------------------------------
#@# Sphere rh Sat May 21 19:34:27 EDT 2011

 mris_sphere -seed 1234 ../surf/rh.inflated ../surf/rh.sphere 

#--------------------------------------------
#@# Surf Reg rh Sat May 21 20:43:16 EDT 2011

 mris_register -curv ../surf/rh.sphere /space/freesurfer/centos4.0_x86_64/stable5/average/rh.average.curvature.filled.buckner40.tif ../surf/rh.sphere.reg 

#--------------------------------------------
#@# Jacobian white rh Sat May 21 21:18:04 EDT 2011

 mris_jacobian ../surf/rh.white ../surf/rh.sphere.reg ../surf/rh.jacobian_white 

#--------------------------------------------
#@# AvgCurv rh Sat May 21 21:18:06 EDT 2011

 mrisp_paint -a 5 /space/freesurfer/centos4.0_x86_64/stable5/average/rh.average.curvature.filled.buckner40.tif#6 ../surf/rh.sphere.reg ../surf/rh.avg_curv 

#-----------------------------------------
#@# Cortical Parc rh Sat May 21 21:18:09 EDT 2011

 mris_ca_label -l ../label/rh.cortex.label -aseg ../mri/aseg.mgz -seed 1234 good_output rh ../surf/rh.sphere.reg /space/freesurfer/centos4.0_x86_64/stable5/average/rh.curvature.buckner40.filled.desikan_killiany.2010-03-25.gcs ../label/rh.aparc.annot 

#--------------------------------------------
#@# Make Pial Surf rh Sat May 21 21:18:58 EDT 2011

 mris_make_surfaces -white NOWRITE -mgz -T1 brain.finalsurfs good_output rh 

#--------------------------------------------
#@# Surf Volume rh Sat May 21 21:30:05 EDT 2011

 mris_calc -o rh.area.mid rh.area add rh.area.pial 


 mris_calc -o rh.area.mid rh.area.mid div 2 


 mris_calc -o rh.volume rh.area.mid mul rh.thickness 

#-----------------------------------------
#@# Parcellation Stats rh Sat May 21 21:30:06 EDT 2011

 mris_anatomical_stats -mgz -cortex ../label/rh.cortex.label -f ../stats/rh.aparc.stats -b -a ../label/rh.aparc.annot -c ../label/aparc.annot.ctab good_output rh white 

#-----------------------------------------
#@# Cortical Parc 2 rh Sat May 21 21:30:20 EDT 2011

 mris_ca_label -l ../label/rh.cortex.label -aseg ../mri/aseg.mgz -seed 1234 good_output rh ../surf/rh.sphere.reg /space/freesurfer/centos4.0_x86_64/stable5/average/rh.destrieux.simple.2009-07-29.gcs ../label/rh.aparc.a2009s.annot 

#-----------------------------------------
#@# Parcellation Stats 2 rh Sat May 21 21:31:19 EDT 2011

 mris_anatomical_stats -mgz -cortex ../label/rh.cortex.label -f ../stats/rh.aparc.a2009s.stats -b -a ../label/rh.aparc.a2009s.annot -c ../label/aparc.annot.a2009s.ctab good_output rh white 

#--------------------------------------------
#@# Cortical ribbon mask Sat May 21 21:31:36 EDT 2011

 mris_volmask --label_left_white 2 --label_left_ribbon 3 --label_right_white 41 --label_right_ribbon 42 --save_ribbon good_output 

#--------------------------------------------
#@# ASeg Stats Sat May 21 21:45:38 EDT 2011

 mri_segstats --seg mri/aseg.mgz --sum stats/aseg.stats --pv mri/norm.mgz --empty --excludeid 0 --excl-ctxgmwm --supratent --subcortgray --in mri/norm.mgz --in-intensity-name norm --in-intensity-units MR --etiv --surf-wm-vol --surf-ctx-vol --totalgray --ctab /space/freesurfer/centos4.0_x86_64/stable5/ASegStatsLUT.txt --subject good_output 

#-----------------------------------------
#@# AParc-to-ASeg Sat May 21 21:55:34 EDT 2011

 mri_aparc2aseg --s good_output --volmask 


 mri_aparc2aseg --s good_output --volmask --a2009s 

#-----------------------------------------
#@# WMParc Sat May 21 21:58:09 EDT 2011

 mri_aparc2aseg --s good_output --labelwm --hypo-as-wm --rip-unknown --volmask --o mri/wmparc.mgz --ctxseg aparc+aseg.mgz 


 mri_segstats --seg mri/wmparc.mgz --sum stats/wmparc.stats --pv mri/norm.mgz --excludeid 0 --brain-vol-from-seg --brainmask mri/brainmask.mgz --in mri/norm.mgz --in-intensity-name norm --in-intensity-units MR --subject good_output --surf-wm-vol --ctab /space/freesurfer/centos4.0_x86_64/stable5/WMParcStatsLUT.txt --etiv 

#--------------------------------------------
#@# BA Labels lh Sat May 21 22:15:57 EDT 2011

 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.BA1.label --trgsubject good_output --trglabel ./lh.BA1.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.BA2.label --trgsubject good_output --trglabel ./lh.BA2.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.BA3a.label --trgsubject good_output --trglabel ./lh.BA3a.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.BA3b.label --trgsubject good_output --trglabel ./lh.BA3b.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.BA4a.label --trgsubject good_output --trglabel ./lh.BA4a.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.BA4p.label --trgsubject good_output --trglabel ./lh.BA4p.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.BA6.label --trgsubject good_output --trglabel ./lh.BA6.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.BA44.label --trgsubject good_output --trglabel ./lh.BA44.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.BA45.label --trgsubject good_output --trglabel ./lh.BA45.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.V1.label --trgsubject good_output --trglabel ./lh.V1.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.V2.label --trgsubject good_output --trglabel ./lh.V2.label --hemi lh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/lh.MT.label --trgsubject good_output --trglabel ./lh.MT.label --hemi lh --regmethod surface 


 mris_label2annot --s good_output --hemi lh --ctab /space/freesurfer/centos4.0_x86_64/stable5/average/colortable_BA.txt --l lh.BA1.label --l lh.BA2.label --l lh.BA3a.label --l lh.BA3b.label --l lh.BA4a.label --l lh.BA4p.label --l lh.BA6.label --l lh.BA44.label --l lh.BA45.label --l lh.V1.label --l lh.V2.label --l lh.MT.label --a BA --maxstatwinner --noverbose 


 mris_anatomical_stats -mgz -f ../stats/lh.BA.stats -b -a ./lh.BA.annot -c ./BA.ctab good_output lh white 

#--------------------------------------------
#@# BA Labels rh Sat May 21 22:18:03 EDT 2011

 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.BA1.label --trgsubject good_output --trglabel ./rh.BA1.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.BA2.label --trgsubject good_output --trglabel ./rh.BA2.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.BA3a.label --trgsubject good_output --trglabel ./rh.BA3a.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.BA3b.label --trgsubject good_output --trglabel ./rh.BA3b.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.BA4a.label --trgsubject good_output --trglabel ./rh.BA4a.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.BA4p.label --trgsubject good_output --trglabel ./rh.BA4p.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.BA6.label --trgsubject good_output --trglabel ./rh.BA6.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.BA44.label --trgsubject good_output --trglabel ./rh.BA44.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.BA45.label --trgsubject good_output --trglabel ./rh.BA45.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.V1.label --trgsubject good_output --trglabel ./rh.V1.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.V2.label --trgsubject good_output --trglabel ./rh.V2.label --hemi rh --regmethod surface 


 mri_label2label --srcsubject fsaverage --srclabel /autofs/space/birn_045/users/BWH/buckner_data/tutorial_subjs/fsaverage/label/rh.MT.label --trgsubject good_output --trglabel ./rh.MT.label --hemi rh --regmethod surface 


 mris_label2annot --s good_output --hemi rh --ctab /space/freesurfer/centos4.0_x86_64/stable5/average/colortable_BA.txt --l rh.BA1.label --l rh.BA2.label --l rh.BA3a.label --l rh.BA3b.label --l rh.BA4a.label --l rh.BA4p.label --l rh.BA6.label --l rh.BA44.label --l rh.BA45.label --l rh.V1.label --l rh.V2.label --l rh.MT.label --a BA --maxstatwinner --noverbose 


 mris_anatomical_stats -mgz -f ../stats/rh.BA.stats -b -a ./rh.BA.annot -c ./BA.ctab good_output rh white 

#--------------------------------------------
#@# Ex-vivo Entorhinal Cortex Label lh Sat May 21 22:20:11 EDT 2011

 mris_spherical_average -erode 1 -orig white -t 0.4 -o good_output label lh.entorhinal lh sphere.reg lh.EC_average lh.entorhinal_exvivo.label 


 mris_anatomical_stats -mgz -f ../stats/lh.entorhinal_exvivo.stats -b -l ./lh.entorhinal_exvivo.label good_output lh white 

#--------------------------------------------
#@# Ex-vivo Entorhinal Cortex Label rh Sat May 21 22:20:29 EDT 2011

 mris_spherical_average -erode 1 -orig white -t 0.4 -o good_output label rh.entorhinal rh sphere.reg rh.EC_average rh.entorhinal_exvivo.label 


 mris_anatomical_stats -mgz -f ../stats/rh.entorhinal_exvivo.stats -b -l ./rh.entorhinal_exvivo.label good_output rh white 

