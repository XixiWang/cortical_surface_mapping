# MapIcosahedron generated spec file
#History: [xixiwang@Xixis-MacBook-Pro.local: Tue Jun 23 17:42:24 2015] MapIcosahedron -spec good_output_lh.spec -ld 141 -dset_map lh.thickness.gii.dset -dset_map lh.curv.gii.dset -dset_map lh.sulc.gii.dset -prefix std.141.

#define the group
	Group = good_output

#define various States
	StateDef = std.smoothwm
	StateDef = std.pial
	StateDef = std.inflated
	StateDef = std.sphere
	StateDef = std.white
	StateDef = std.sphere.reg

NewSurface
	SurfaceFormat = ASCII
	SurfaceType = FreeSurfer
	SurfaceName = ./std.141.lh.smoothwm.asc
	LocalDomainParent = ./SAME
	LabelDset = ./std.141.lh.aparc.a2009s.annot.niml.dset
	SurfaceState = std.smoothwm
	EmbedDimension = 3
	Anatomical = Y
	LocalCurvatureParent = ./SAME

NewSurface
	SurfaceFormat = ASCII
	SurfaceType = FreeSurfer
	SurfaceName = ./std.141.lh.pial.asc
	LocalDomainParent = ./std.141.lh.smoothwm.asc
	SurfaceState = std.pial
	EmbedDimension = 3
	Anatomical = Y
	LocalCurvatureParent = ./std.141.lh.smoothwm.asc

NewSurface
	SurfaceFormat = ASCII
	SurfaceType = FreeSurfer
	SurfaceName = ./std.141.lh.inflated.asc
	LocalDomainParent = ./std.141.lh.smoothwm.asc
	SurfaceState = std.inflated
	EmbedDimension = 3
	Anatomical = N
	LocalCurvatureParent = ./std.141.lh.smoothwm.asc

NewSurface
	SurfaceFormat = ASCII
	SurfaceType = FreeSurfer
	SurfaceName = ./std.141.lh.sphere.asc
	LocalDomainParent = ./std.141.lh.smoothwm.asc
	SurfaceState = std.sphere
	EmbedDimension = 3
	Anatomical = N
	LocalCurvatureParent = ./std.141.lh.smoothwm.asc

NewSurface
	SurfaceFormat = ASCII
	SurfaceType = FreeSurfer
	SurfaceName = ./std.141.lh.white.asc
	LocalDomainParent = ./std.141.lh.smoothwm.asc
	SurfaceState = std.white
	EmbedDimension = 3
	Anatomical = Y
	LocalCurvatureParent = ./std.141.lh.smoothwm.asc

NewSurface
	SurfaceFormat = ASCII
	SurfaceType = FreeSurfer
	SurfaceName = ./std.141.lh.sphere.reg.asc
	LocalDomainParent = ./std.141.lh.smoothwm.asc
	SurfaceState = std.sphere.reg
	EmbedDimension = 3
	Anatomical = N
	LocalCurvatureParent = ./std.141.lh.smoothwm.asc
