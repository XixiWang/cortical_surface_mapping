# delimits comments

# Creation information:
#     user    : xixiwang
#     date    : Tue Jun 23 17:42:18 EDT 2015
#     machine : Xixis-MacBook-Pro.local
#     pwd     : /Users/xixiwang/Dropbox/Cortical_Surface_Map/good_output/surf/SUMA
#     command : @SUMA_Make_Spec_FS -sid good_output

# define the group
        Group = good_output

# define various States
        StateDef = smoothwm
        StateDef = pial
        StateDef = inflated_lh
        StateDef = inflated_rh
        StateDef = occip.patch.3d
        StateDef = occip.patch.flat_lh
        StateDef = occip.patch.flat_rh
        StateDef = occip.flat.patch.3d_lh
        StateDef = occip.flat.patch.3d_rh
        StateDef = fusiform.patch.flat_lh
        StateDef = fusiform.patch.flat_rh
        StateDef = full.patch.3d
        StateDef = full.patch.flat_lh
        StateDef = full.patch.flat_rh
        StateDef = full.flat.patch.3d_lh
        StateDef = full.flat.patch.3d_rh
        StateDef = full.flat_lh
        StateDef = full.flat_rh
        StateDef = flat.patch_lh
        StateDef = flat.patch_rh
        StateDef = sphere_lh
        StateDef = sphere_rh
        StateDef = white
        StateDef = sphere.reg_lh
        StateDef = sphere.reg_rh
        StateDef = rh.sphere.reg_lh
        StateDef = rh.sphere.reg_rh
        StateDef = lh.sphere.reg_lh
        StateDef = lh.sphere.reg_rh
        StateDef = pial-outer-smoothed

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.smoothwm.asc
        LocalDomainParent = SAME
        SurfaceState = smoothwm
        EmbedDimension = 3
        Anatomical = Y
        LabelDset = lh.aparc.a2009s.annot.niml.dset

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.pial.asc
        LocalDomainParent = lh.smoothwm.asc
        SurfaceState = pial
        EmbedDimension = 3
        Anatomical = Y

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.inflated.asc
        LocalDomainParent = lh.smoothwm.asc
        SurfaceState = inflated_lh
        EmbedDimension = 3
        Anatomical = N

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.sphere.asc
        LocalDomainParent = lh.smoothwm.asc
        SurfaceState = sphere_lh
        EmbedDimension = 3
        Anatomical = N

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.white.asc
        LocalDomainParent = lh.smoothwm.asc
        SurfaceState = white
        EmbedDimension = 3
        Anatomical = Y

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.sphere.reg.asc
        LocalDomainParent = lh.smoothwm.asc
        SurfaceState = sphere.reg_lh
        EmbedDimension = 3
        Anatomical = N

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = rh.smoothwm.asc
        LocalDomainParent = SAME
        SurfaceState = smoothwm
        EmbedDimension = 3
        Anatomical = Y
        LabelDset = rh.aparc.a2009s.annot.niml.dset

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = rh.pial.asc
        LocalDomainParent = rh.smoothwm.asc
        SurfaceState = pial
        EmbedDimension = 3
        Anatomical = Y

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = rh.inflated.asc
        LocalDomainParent = rh.smoothwm.asc
        SurfaceState = inflated_rh
        EmbedDimension = 3
        Anatomical = N

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = rh.sphere.asc
        LocalDomainParent = rh.smoothwm.asc
        SurfaceState = sphere_rh
        EmbedDimension = 3
        Anatomical = N

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = rh.white.asc
        LocalDomainParent = rh.smoothwm.asc
        SurfaceState = white
        EmbedDimension = 3
        Anatomical = Y

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = rh.sphere.reg.asc
        LocalDomainParent = rh.smoothwm.asc
        SurfaceState = sphere.reg_rh
        EmbedDimension = 3
        Anatomical = N

