# delimits comments

# Creation information:
#     user    : xixiwang
#     date    : Tue Jun 23 17:42:18 EDT 2015
#     machine : Xixis-MacBook-Pro.local
#     pwd     : /Users/xixiwang/Dropbox/Cortical_Surface_Map/good_output/surf/SUMA
#     command : @SUMA_Make_Spec_FS -sid good_output

# define the group
        Group = good_output

# define various States
        StateDef = smoothwm
        StateDef = pial
        StateDef = inflated
        StateDef = occip.patch.3d
        StateDef = occip.patch.flat
        StateDef = occip.flat.patch.3d
        StateDef = fusiform.patch.flat
        StateDef = full.patch.3d
        StateDef = full.patch.flat
        StateDef = full.flat.patch.3d
        StateDef = full.flat
        StateDef = flat.patch
        StateDef = sphere
        StateDef = white
        StateDef = sphere.reg
        StateDef = rh.sphere.reg
        StateDef = lh.sphere.reg
        StateDef = pial-outer-smoothed

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.smoothwm.asc
        LocalDomainParent = SAME
        SurfaceState = smoothwm
        EmbedDimension = 3
        Anatomical = Y
        LabelDset = lh.aparc.a2009s.annot.niml.dset

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.pial.asc
        LocalDomainParent = lh.smoothwm.asc
        SurfaceState = pial
        EmbedDimension = 3
        Anatomical = Y

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.inflated.asc
        LocalDomainParent = lh.smoothwm.asc
        SurfaceState = inflated
        EmbedDimension = 3
        Anatomical = N

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.sphere.asc
        LocalDomainParent = lh.smoothwm.asc
        SurfaceState = sphere
        EmbedDimension = 3
        Anatomical = N

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.white.asc
        LocalDomainParent = lh.smoothwm.asc
        SurfaceState = white
        EmbedDimension = 3
        Anatomical = Y

NewSurface
        SurfaceFormat = ASCII
        SurfaceType = FreeSurfer
        FreeSurferSurface = lh.sphere.reg.asc
        LocalDomainParent = lh.smoothwm.asc
        SurfaceState = sphere.reg
        EmbedDimension = 3
        Anatomical = N

