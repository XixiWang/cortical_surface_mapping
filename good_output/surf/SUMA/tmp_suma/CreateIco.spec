# SUMA_CreateIcosahedron-main generated spec file
#History: [xwang91@imac-2b201e.local: Thu Jul  9 09:47:25 2015] CreateIcosahedron -rd 4

#define the group
	Group = Icosahedron

#define various States
	StateDef = icos.854vert.1706tri

NewSurface
	SurfaceFormat = ASCII
	SurfaceType = FreeSurfer
	SurfaceName = CreateIco.asc
	LocalDomainParent = ./SAME
	SurfaceState = icos.854vert.1706tri
	EmbedDimension = 3
	Anatomical = N
