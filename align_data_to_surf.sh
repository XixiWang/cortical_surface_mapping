#! /bin/bash

export datadir=~/Documents/Code/cortical_surface_mapping/good_output/surf/SUMA
cd ${datadir}

# Project dataset onto 2D surface
3dVol2Surf -spec good_output_both.spec \
    -sv good_output_SurfVol+orig \
    -grid_parent roi_med_taua_dax_post_gabor.nii \
    -map_func ave \
    -out_1D ~/Desktop/Test_taua_dax.1D


3dVol2Surf -spec good_output_rh.spec \
    -surf_A rh.smoothwm.asc \
    -surf_B rh.pial.asc \
    -sv good_output_SurfVol+orig \
    -map_func ave \
    -grid_parent roi_med_taua_dax_pre_kernel.nii \
    -out_niml test_out_kernel2.niml.dset

suma -spec good_output_rh.spec -sv good_output_SurfVol+orig.

## For single hemisphere and needs to be rescaled!! 
