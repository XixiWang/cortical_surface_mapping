clear;
% clc;

addpath('/Applications/freesurfer/matlab/');
addpath('~/Dropbox/Cortical_Surface_Map/good_output/');
addpath('~/Dropbox/Cortical_Surface_Map/');

%% Load original template
disp('Analyzing left hemisphere');
Filename = '~/Dropbox/Cortical_Surface_Map/good_output/label/lh.aparc.a2009s.annot';
[vertices, label, colortable] = read_annotation(Filename);
template_ROI_name = colortable.struct_names;
template_ROI_name = strcat('ctx_lh_',template_ROI_name);
template_ROI_name{1,1} = 'Unknown';
template_data = [template_ROI_name num2cell(colortable.table)];

%% Load decoding result
text = importdata('figure4_predicted_sent_vs_observed_sent_matching_mean_acc_per_roi.xlsx');
mean_val = text.data(:,1);
ROI_name = text.textdata(2:end,1);

% Convert to RGB values
colormap parula;
imagesc(mean_val);
cm = colormap;
close all;

mean_val_vec = mean_val(:);
myColor_double = zeros(length(mean_val_vec),3);
for ctr = 1:length(mean_val_vec)
    colorID = max(1, sum(mean_val_vec(ctr) > [0:1/length(cm(:,1)):1])); 
    myColor_double(ctr,:) = cm(colorID,:);
end
myColor_RGB = uint8(myColor_double * 255);
myColor_RGB_backup = double(myColor_RGB);
structID = myColor_RGB_backup(:,1) + myColor_RGB_backup(:,2) .* 2^8 + myColor_RGB_backup(:,3) .* 2^16;
myData = [ROI_name num2cell(myColor_RGB_backup) num2cell(zeros(length(myColor_RGB),1)) num2cell(structID)];

% Based on the original colortable, find corresponding idx and assign new
% values
template_data_backup = template_data;
for ctr = 2:size(template_data,1)
    idx = ismember(myData(:,1),template_data{ctr,1});
    % See whether could find corresponding label
    if any(idx)
        template_data(ctr,7:12) = myData(idx,:);
    else
        % If this is not in the extracted, set as unknown
        template_data(ctr,7:12) = num2cell(0); 
    end
end
template_data{1,7} = 'Unknown';
template_data(1,8:12) = num2cell(0);

%% Generate new vertices list
label_new = zeros(size(label));
template_data_label = cell2mat(template_data(:,6));
template_data_label_new = cell2mat(template_data(:,12));

for ctr = 1:size(label,1)
    % Find the old index
    [idx,~] = find(template_data_label == label(ctr));
    label_new(ctr) = template_data_label_new(idx);
end

%%
% Write back to colortable
colortable_new = colortable;
colortable_new.table = cell2mat(template_data(:,8:12));
cd('~/Dropbox/Cortical_Surface_Map/good_output/label');
% write_annotation('lh.aparc.a2009s_parula.annot',vertices,label_new,colortable_new);