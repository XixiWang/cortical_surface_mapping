%% Destrieux atlas,fsaverage/mri/aparc.a2009s+aseg.mgz
clear;
close all;

addpath(pwd);
text = importdata('figure4_predicted_sent_vs_observed_sent_matching_mean_acc_per_roi.xlsx');
mean_val = text.data(:,1);
ROI_name = text.textdata(2:end,1);
colormap winter;
imagesc(mean_val);
cm = colormap;
close all;

mean_val_vec = mean_val(:);
myColor_double = zeros(length(mean_val_vec),3);
for ctr = 1:length(mean_val_vec)
    colorID = max(1, sum(mean_val_vec(ctr) > [0:1/length(cm(:,1)):1])); 
    myColor_double(ctr,:) = cm(colorID,:);
end
myColor_RGB = uint8(myColor_double * 255);

DataTable = table(mean_val,myColor_RGB,'RowNames',ROI_name);
tablename = 'RGB_winter.txt';
% writetable(DataTable,tablename,'WriteRowNames',true,'Delimiter','space');

%% Import template data
[~, ~, raw] = xlsread('FreeSurferColorLUT.xlsx','Sheet1');
raw = raw(:,1:6);
raw(cellfun(@(x) ~isempty(x) && isnumeric(x) && isnan(x),raw)) = {''};
cellVectors = raw(:,2);
raw = raw(:,[1,3,4,5,6]);

% Exclude rows with non-numeric cells
J = ~all(cellfun(@(x) (isnumeric(x) || islogical(x)) && ~isnan(x),raw),2); % Find rows with non-numeric cells
raw(J,:) = [];
cellVectors(J,:) = [];

% Create output variable
data = reshape([raw{:}],size(raw));

% Allocate imported array to column variable names
ROI_Idx = data(:,1);
ROI_Names = cellVectors(:,1);
Ori_RGB1 = data(:,2);
Ori_RGB2 = data(:,3);
Ori_RGB3 = data(:,4);
Default_zeros = data(:,5);

% Clear temporary variables
clearvars data raw cellVectors J;

%% Find corresponding idx
TemplateData = cell(1266,2);
for ctr = 1:1266
    TemplateData{ctr,1} = ROI_Idx(ctr);
end
TemplateData(:,2) = ROI_Names;

% CorrIdx = TemplateData{ismember(TemplateData(:,2),'Line-1'),1};

%% Find Corresponding idx
TrueIdx = cell(length(ROI_name),2);
for ctr = 1:length(ROI_name)
    tempIdx = ismember(TemplateData(:,2),ROI_name{ctr,1});
    tempval = TemplateData(tempIdx,1);
    TrueIdx{ctr,1} = tempval{1,1};
    TrueIdx{ctr,2} = TemplateData(tempIdx,2);
end 

%% Attach RGB to TrueIdx cell
SummaryData = {};
SummaryData = [SummaryData,TrueIdx];
SummaryData = [SummaryData,num2cell(myColor_RGB)];
myData_zeros = zeros(length(ROI_name),1);
SummaryData = [SummaryData,num2cell(myData_zeros),num2cell(mean_val)];

%% Output txt file
T = cell2table(SummaryData);
% writetable(T,'Idx_Data_winter.txt','WriteRowNames',true,'Delimiter','space');
