% Color code each ROI based on decoding accuracy

clear;
% clc;
close all;

% code_dir = '~/Dropbox/LAB_Folder_Macbook/MATLAB/code/Cortical_Surface_Mapping';
code_dir = '~/Documents/Code/cortical_surface_mapping';

addpath('/Applications/freesurfer/matlab/');
addpath(code_dir);
addpath(fullfile(code_dir,'good_output'));

%% Load freesurfer template
disp('Analyzing left hemisphere');
% --------------------------------------
Filename = fullfile(code_dir,'good_output/label/rh.aparc.a2009s.annot');
% --------------------------------------

[vertices, label, colortable] = read_annotation(Filename);
template_ROI_name = colortable.struct_names;
% --------------------------------------
template_ROI_name = strcat('ctx_rh_',template_ROI_name);
% --------------------------------------

template_ROI_name{1,1} = 'Unknown';
template_data = [template_ROI_name num2cell(colortable.table)];

%% Load decoding result
% --------------------------------------
% text = importdata('figure2_mean_latentword_vs_model_word_acuracy_per_roi.xlsx');
% text = importdata('figure3_spot_word_in_sent_with_regenerated_word_rankscore_per_roi.xlsx');
text = importdata('figure4_predicted_sent_vs_observed_sent_matching_mean_acc_per_roi.xlsx');
% --------------------------------------

mean_val = text.data(:,1);
ROI_name = text.textdata(2:end,1);

% When converting to RGB values:
% Adust the colorbar
mean_val_min = min(mean_val(:));
mean_val_max = max(mean_val(:));

% Convert to RGB values
colormap parula;
imagesc(mean_val);
cm = colormap;
% close all;
L = size(cm,1);
% Scale original data to match the current map
mean_val_sca = round(interp1(linspace(mean_val_min,mean_val_max,L),1:L,mean_val));
% Make RGB image
H = reshape(cm(mean_val_sca,:),[size(mean_val_sca) 3]);
% Convert to [0,255]
myColor_RGB = uint8(H * 255);
myColor_RGB = squeeze(myColor_RGB);
myColor_RGB_backup = double(myColor_RGB);
structID = myColor_RGB_backup(:,1) + myColor_RGB_backup(:,2) .* 2^8 + myColor_RGB_backup(:,3) .* 2^16;
myData = [ROI_name num2cell(myColor_RGB_backup) num2cell(zeros(length(myColor_RGB),1)) num2cell(structID)];
myData_Backup = [mean_val myColor_RGB_backup];

% --------------------------------------
% Figure 2:
% caxis([0.55 mean_val_max]);
% colorbar('Ticks',[.55,.56,.63,mean_val_max],'TickLabels',{'.55','p=.05','p=.0001','best'},'FontSize',12);

% Figure 3:
% colorbar('Ticks',[.5,.53,.56],'TickLabels',{'chance','p=.05','p=.0001'},'FontSize',12);

% Figure 4:
colorbar('Ticks',[.5,.64,.69],'TickLabels',{'chance','p=.05','p=.01'},'FontSize',12);
% --------------------------------------

% Based on the original colortable, find corresponding idx and assign new
% values
template_data_backup = template_data;
for ctr = 2:size(template_data,1)
    idx = ismember(myData(:,1),template_data{ctr,1});
    % See whether could find corresponding label
    if any(idx)
        template_data(ctr,7:12) = myData(idx,:);
    else
        % If this is not in the extracted, set as unknown
        template_data(ctr,7:12) = num2cell(0); 
    end
end
template_data{1,7} = 'Unknown';
template_data(1,8:12) = num2cell(0);

%% Generate new vertices list
label_new = zeros(size(label));
template_data_label = cell2mat(template_data(:,6));
template_data_label_new = cell2mat(template_data(:,12));

for ctr = 1:size(label,1)
    % Find the old index
    [idx,~] = find(template_data_label == label(ctr));
    label_new(ctr) = template_data_label_new(idx);
end

%%
% Write back to colortable
colortable_new = colortable;
colortable_new.table = cell2mat(template_data(:,8:12));
cd('~/Dropbox/Cortical_Surface_Map/good_output/label');
% --------------------------------------
% write_annotation('lh.aparc.a2009s_parula_fig2.annot',vertices,label_new,colortable_new);
% --------------------------------------
