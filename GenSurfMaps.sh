#! /bin/bash

export PATH={PATH}:/Applications/freesurfer/bin/
export SUBJECTS_DIR=~/Dropbox/Cortical_Surface_Map

echo "Bash script to generate different surface maps"

cd $SUBJECTS_DIR

for nfig in fig2 fig3 fig4
do 
    # Left hemisphere
    mris_convert --annot good_output/label/lh.aparc.a2009s_parula_${nfig}.annot good_output/surf/lh.inflated good_output/surf/SUMA/lh.aparc.a2009s_parula_${nfig}.annot.gii

    # Right hemisphere
    mris_convert --annot good_output/label/rh.aparc.a2009s_parula_${nfig}.annot good_output/surf/rh.inflated good_output/surf/SUMA/rh.aparc.a2009s_parula_${nfig}.annot.gii

    echo "${nfig} is done!"
done

# cd $good_output/surf/SUMA
# suma -spec good_output_both.spec

suma -spec good_output_lh.spec -drive_com '-com surf_cont -view_surf_cont y -load_dset lh.aparc.a2009s_parula_fig2.annot.gii -Dim 0.6'
